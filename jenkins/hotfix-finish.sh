#!/bin/bash -e

HOTFIX_BRANCH=origin/$1
TAG_VERSION=$2
MAIN_BRANCH=master
MERGE_BRANCH=$3

echo checkout ${MAIN_BRANCH}
git checkout ${MAIN_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to checkout ${MAIN_BRANCH}
  exit 1
fi

echo merge ${HOTFIX_BRANCH} into ${MAIN_BRANCH}
git merge --no-ff ${HOTFIX_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to merge ${HOTFIX_BRANCH} into ${MAIN_BRANCH}
  exit 1
fi

echo tag ${TAG_VERSION}
git tag -m "tag ${TAG_VERSION}" -a ${TAG_VERSION}

if [ $? -ne 0 ]
then
  echo unable to tag ${TAG_VERSION}
  exit 1
fi

echo checkout ${MERGE_BRANCH}
git checkout ${MERGE_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to checkout ${MERGE_BRANCH}
  exit 1
fi

echo merge ${HOTFIX_BRANCH} into ${MERGE_BRANCH}
git merge --no-ff ${HOTFIX_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to merge ${HOTFIX_BRANCH} into ${MERGE_BRANCH}
  exit 1
fi

echo push all changes to origin
git push --all origin

if [ $? -ne 0 ]
then
  echo unable to push all changes to origin
  exit 1
fi

echo push ${TAG_VERSION} tag to origin
git push origin ${TAG_VERSION}

if [ $? -ne 0 ]
then
  echo unable to push ${TAG_VERSION} tag to origin
  exit 1
fi
