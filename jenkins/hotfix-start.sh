#!/bin/bash -e

HOTFIX_VERSION=$1
HOTFIX_BRANCH="hot-fix-${HOTFIX_VERSION}"
MAIN_BRANCH=master

echo checkout ${MAIN_BRANCH}
git checkout ${MAIN_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to checkout ${MAIN_BRANCH}
  exit 1
fi

echo create ${HOTFIX_BRANCH} from ${MAIN_BRANCH}
git checkout -b ${HOTFIX_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to create ${HOTFIX_BRANCH} from ${MAIN_BRANCH}
  exit 1
fi

echo update ${HOTFIX_BRANCH} version to ${HOTFIX_VERSION}
./mvnw versions:set -DnewVersion="${HOTFIX_VERSION}"

if [ $? -ne 0 ]
then
  echo unable to update ${HOTFIX_BRANCH} version to ${HOTFIX_VERSION}
  exit 1
fi

echo commit hotfix version to ${HOTFIX_BRANCH}
git commit -a -m "Update pom version to ${HOTFIX_VERSION}"

if [ $? -ne 0 ]
then
  echo unable to commit hotfix version to ${HOTFIX_BRANCH}
  exit 1
fi

echo checkout develop
git checkout develop

if [ $? -ne 0 ]
then
  echo unable to checkout develop
  exit 1
fi

echo merge ${HOTFIX_BRANCH} to develop with -s flag to avoid pom version number conflicts down the road
git merge --no-edit -s ours ${HOTFIX_BRANCH}

if [ $? -ne 0 ]
then
  echo unable to merge ${HOTFIX_BRANCH} to develop with -s flag to avoid pom version number conflicts down the road
  exit 1
fi

echo push all changes to origin
git push --all origin

if [ $? -ne 0 ]
then
  echo unable to push all changes to origin
  exit 1
fi
