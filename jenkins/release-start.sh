#!/bin/bash -e

RELEASE_BRANCH=$1
NEXT_VERSION=$2
RELEASE_VERSION="${RELEASE_BRANCH}.0"

echo checkout develop
git checkout develop

if [ $? -ne 0 ]
then
  echo unable to checkout develop
  exit 1
fi

echo update develop branch to ${RELEASE_VERSION}
./mvnw versions:set -DnewVersion="${RELEASE_VERSION}"

if [ $? -ne 0 ]
then
  echo unable to update develop branch to ${RELEASE_VERSION}
  exit 1
fi

echo commit release version to develop
git commit -a -m "Update pom version to ${RELEASE_VERSION}"

if [ $? -ne 0 ]
then
  echo unable to commit release version to develop
  exit 1
fi

echo create release-${RELEASE_BRANCH}
git checkout -b "release-${RELEASE_BRANCH}"

if [ $? -ne 0 ]
then
  echo unable to create release-${RELEASE_BRANCH}
  exit 1
fi

echo checkout develop
git checkout develop

if [ $? -ne 0 ]
then
  echo unable to checkout develop
  exit 1
fi

echo update develop version to ${NEXT_VERSION}
./mvnw versions:set -DnewVersion="${NEXT_VERSION}"

if [ $? -ne 0 ]
then
  echo unable to update develop version to ${NEXT_VERSION}
  exit 1
fi

echo commit updated develop version
git commit -a -m "Update pom version to ${NEXT_VERSION}"

if [ $? -ne 0 ]
then
  echo unable to commit updated develop version
  exit 1
fi

echo push all changes to origin
git push --all origin

if [ $? -ne 0 ]
then
  echo unable to push all changes to origin
  exit 1
fi
