@ECHO OFF
ECHO Initializing the Performance Execution 
SET HOUR=%time:~0,2%
SET dtStamp9=%date:~10,4%%date:~4,2%%date:~7,2%_0%time:~1,1%%time:~3,2%%time:~6,2% 
SET dtStamp24=%date:~10,4%%date:~4,2%%date:~7,2%_%time:~0,2%%time:~3,2%%time:~6,2%
if "%HOUR:~0,1%" == " " (SET dtStamp=%dtStamp9%) else (SET dtStamp=%dtStamp24%)
ECHO Performance execution initialization complete
TIMEOUT /T 10
ECHO Performance execution Started Succesfully for timestamp %dtStamp%

jmeter -n -t ./Scripts/eSignLoadTest.jmx -l ./Results/Results_%dtStamp%.jtl -j ./Log/ScriptLogs_%dtStamp%.log -e -o ./Report/RunReport_%dtStamp%

TIMEOUT /T 5
ECHO Run completed Succesfully. Creating Run Reports For -  %dtStamp%
TIMEOUT /T 10
cd Log
ren SummaryReport.jtl SummaryReport_%dtStamp%.jtl 
ren ViewResultsTree.jtl ViewResultsTree_%dtStamp%.jtl

cd ..
cd ..

ECHO Report files Created with suffix -  %dtStamp%
PAUSE
