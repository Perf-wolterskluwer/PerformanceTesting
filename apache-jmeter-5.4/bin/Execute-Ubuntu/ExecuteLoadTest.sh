#!/bin/bash
echo Initializing the Performance Execution 
echo "Script executed from: ${PWD}"
timestamp=$(date +%s)
echo Performance execution initialization complete
echo Performance execution Started Succesfully for timestamp $timestamp
export PATH="$PATH":./apache-jmeter-5.4/bin/

cd ./apache-jmeter-5.4/bin/

./jmeter -n -t ./Scripts/eSignLoadTest.jmx -l ./Results/Results_$timestamp.jtl -j ./Log/ScriptLogs_$timestamp.log -e -o ./Report/RunReport_$timestamp

echo Run completed Succesfully. Creating Run Reports For -  $timestamp
cd Log
mv SummaryReport.jtl SummaryReport_$timestamp.jtl 
mv ViewResultsTree.jtl ViewResultsTree_$timestamp.jtl
cd ..
cd ..
echo Report files Created with suffix -  $timestamp
read -p "Run completed Successfully"
