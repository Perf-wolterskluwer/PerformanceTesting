### Prerequisite

* Java 1.8+
* Apache JMeter 5.3+
* Docker (optional)

### Enviorment Variables Required

| **Variable Name** 		| **Sample Value**           	| **Description**  |
| :------------- 			|:-------------:				| :-----|
| pUserCount				|300							|Total number of Concurrent Users|
| execution_duration		|1800							|SteadyState time in Secs|
| pRampUp					|2								|Ramp up time per user in secs|
| HOST_NAME					|dev-smartsign.eoriginal.com	|Url of the Application under test|
| PORT						|								|Port number for Local Application test,Leave blank if N/A|
| p_ClientId				|CRfgn8vNQqVFUoM7sEBL3imr 		|Client Id used in Application|
| pPath						|./Data/Files/ 					|Default path of Data files to be used for upload functionality|
| threadDelay_deviation		|2000 							|Deviation for the wait/pause time between actions|
| threadDelay_constantDelay	|3000							|Average wait/pause time between actions|
| PROTOCOL					|https 							|Protocol to be used for Application under test|
| pExecutionEnv				|dev 							|Enviorment of the application, used to pick % configurations|

### How to execute test suite

```sh
app\apache-jmeter-5.4\bin\Execute-Ubuntu> ExecuteLoadTest.sh
```

### How to build the image

```sh
app> docker build -t esign_performance_testing:latest .
```

### How to run the image

```sh
app> docker run --env-file enviormentVariables.txt esign_performance_testing:latest env
```
