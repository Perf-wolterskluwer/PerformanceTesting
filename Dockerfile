FROM public.ecr.aws/docker/library/ubuntu:focal

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/New_York

RUN apt-get update \
    && apt-get install -y apt-utils \
    build-essential \
    openjdk-8-jdk \
    && apt-get clean
RUN apt-get update && apt-get install -y dos2unix

WORKDIR /app
COPY apache-jmeter-5.4 ./apache-jmeter-5.4
COPY docker-entrypoint.sh /entrypoint.sh

RUN dos2unix /entrypoint.sh && apt-get --purge remove -y dos2unix && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/entrypoint.sh"]